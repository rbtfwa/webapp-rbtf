import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import './index.css';
import Home from "./Home";
import FAQ from "./FAQ";
import Help from "./Help";



ReactDOM.render(
    <BrowserRouter>
        <Home/>
        <FAQ/>
        <Help/>
    </BrowserRouter>,
    document.getElementById('root'));
