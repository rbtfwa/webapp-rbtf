import React from "react";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Header from "./Header";
import ToolbarNavigation from "./ToolbarNavigation";


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}


export default function Help() {
    return(
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="lg">
                <Header/>
                <ToolbarNavigation/>
                <center>
                <h1><u><b>Help Page - Useful Videos</b></u></h1>

                <div className="row">
                <div className="column" >
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/vfqvzgkYYF0" frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                        <p>In this video you will learn useful tips on the best practices on how to improve your code writing.</p>
                        <div className="column">
                            <h3><u> Writing Readable Code </u></h3>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/SPlS4kW0UbE"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                            <p> This is a very useful video, outlining the best ways to improve the readability of your code</p>
                        </div>
                        <div className="column" >
                            <h3><u>10 Tips for Clean Code</u></h3>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/UjhX2sVf0eg"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                            <p>One we here at RBTF have found could be very useful, making sure you format your code to a proffessional level.</p>
                            <script>
                        </script>
                        </div>
                        <div className="column" >
                            <h3><u>Clean Code: Learn to write clean, maintable and robust code</u></h3>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/5koPpYVa020"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                            <p>This video focuses on making sure the code you write is maintainable and robust, so if you are collaborating
                                in a team you should definitely check it out! </p>
                        </div>
                        <div className="column" >
                            <h3><u>Clean Code : Functions</u></h3>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RR_dQ4sBSBM"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                            <p>Have a look at this video if you want to know ways to write much more readable functions, it will come in
                            very helpful!</p>
                        </div>
                        <div className="column" >
                        <h3><u>LOOK AT OUR TEAM! :D</u></h3>

                            <iframe width="560" height="600" src="https://files.slack.com/files-pri/TPDL2N9FA-FQQG6T82E/snapchat-634422807.jpg"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                    </div>
                    </div>
                </div>
                </center>
                <Copyright />
            </Container>
        </React.Fragment>
    )
}
