import Toolbar from "@material-ui/core/Toolbar";
import React from "react";

import {makeStyles} from "@material-ui/core";


const useStyles = makeStyles(theme => ({
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
        flex: 1,
    },
    toolbarSecondary: {
        justifyContent: 'space-between',
        overflowX: 'auto',
    },
    toolbarLink: {
        padding: theme.spacing(1),
        flexShrink: 0,
    }
}));

// const sections = [
//     'Home',
//     'FAQ',
//     'Help'
// ];



export default function ToolbarNavigation() {
    const classes = useStyles();
    return(
        <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
        <a href="Home.js" className="w3-bar-item w3-button w3-padding-large" onClick="myFunction()">Home</a>
        <a href="FAQ.js" className="w3-bar-item w3-button w3-padding-large" onClick="myFunction()">FAQ</a>
        <a href="Help.js" className="w3-bar-item w3-button w3-padding-large" onClick="myFunction()">Help</a>
        </Toolbar>


);
}

// {sections.map(section => (
//     <Link
//         color="inherit"
//         noWrap
//         key={section}
//         variant="body2"
//         href="#"
//         className={classes.toolbarLink}
//     >
//         {section}
//     </Link>
// ))}
