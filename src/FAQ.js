import React from "react";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Header from "./Header";
import ToolbarNavigation from "./ToolbarNavigation";


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}
export default function FAQ() {
    return(
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="lg">
                <Header/>
                <ToolbarNavigation/>
                    <h2 className="w3-wide">READ BETWEEN THE FACTS</h2>
                    <p className="w3-opacity"><i>Making code more Readable</i></p>
                <h3>Frequently Asked Questions</h3>

                <div className="row">
                    <div className="column" >
                        <div className="column">
                            <h4>What Languages are compatible with the Error Checker?</h4>
                            <p>The Error Checker caters for all popular languages such as Java, C code etc. If you notice any issues with your preferred language please feel free to let us know </p>
                        </div>
                        <div className="column" >
                            <h4>How to install Plugins?</h4>
                            <p>We have made the process of installing plugins easier for our users by compiling some of the more popular plugins in this site to save you time searching the internet for them</p>
                        </div>
                        <div className="column" >
                            <h4>Are there any Tutorials for writing Readable Code?</h4>
                            <p>Yes there are! In our help section of this website we have provided a link to Youtube tutorials that will ingrain you with the skills needed to subconsciously begin writing more readable code thus saving you both time and effort </p>
                        </div>
                        <div className="column" >
                            <h4>Will this fix errors in my code?</h4>
                            <p>Unfortunately while this tool is incredibly useful for making your code more readable it unable to detect or repair functional errors within your code</p>
                        </div>
                        <div className="column" >
                            <h4>Will I be able to ask people questions about my code?</h4>
                            <p>Yes you will! You'll be able to connect to other users via the Help section of the website where you will have access to the wisdom of more experienced users and be able to assist the more novice users</p>
                        </div>
                        <div className="column" >
                            <h4>Will I be able to format my entire project at once?</h4>
                            <p>You can! Thanks to the various code viewers that we have provided on our homepage you will be able to find a code viewer that suits your projects needs and make formatting it a breeze</p>
                        </div>
                    </div>
                </div>
                <Copyright />
            </Container>
        </React.Fragment>
    )
}
