import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import Container from "@material-ui/core/Container";
import Header from "./Header";
import ToolbarNavigation from "./ToolbarNavigation";

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://material-ui.com/">
        Your Website
    </Link>{' '}
    {new Date().getFullYear()}
    {'.'}
</Typography>
);
}
const useStyles = makeStyles(theme => ({
    mainGrid: {
        marginTop: theme.spacing(3),
    },
    sidebarAboutBox: {
        padding: theme.spacing(2),
        backgroundColor: theme.palette.grey[200],
    },
    sidebarSection: {
        marginTop: theme.spacing(3),
    },
    drop_zone: {
    padding: 20
}
}));

export default function Home() {

    const classes = useStyles();

    return (
        <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg">
        <Header/>
        <ToolbarNavigation/>
        <center>
            <Typography>
               Regardless of programming language, all these bullterpoints contribute to the readability of code:
            </Typography>
            <Typography>
                -Good variable, method and class names
            </Typography>
            <Typography>
                -Variables, classes and methods that have a single purpose
            </Typography>
            <Typography>
                -Consistent indentation and formatting style
            </Typography>
            <Typography>
                -Reduction of the nesting level in code
            </Typography>
        </center>

            <main>
        {/* Main featured post */}
    {/* End sub featured posts */}
<Grid container spacing={5} className={classes.mainGrid}>

        {/* Main content */}
        <Grid item xs={12} md={8}>

        <Typography variant="h6" gutterBottom>
    Enter your code here to make it more readable!
    </Typography>
            <Typography>
            <a href="http://localhost:63342/webapp-rbtf/Ace.html?_ijt=pasgmc8kvmteqr3inh1crvkt55" className="w3-bar-item w3-button w3-padding-large" onClick="myFunction()">Click here to use  our error checker for your code :)</a>
            </Typography>
        <Typography>
            <a href="http://localhost:63342/webapp-rbtf/index.html?_ijt=eius2pvuq39rcr04l56m4hv9q0" className="w3-bar-item w3-button w3-padding-large" onClick="myFunction()">Click here for more readable code :)</a>
        </Typography>
        </Grid>
    <Divider />
    {/* End main content */}
    {/* Sidebar */}

<Grid item xs={12} md={4}>

        <Paper elevation={0} className={classes.sidebarAboutBox}>
        <Typography variant="h6" gutterBottom>
    Quick Links!
    </Typography>
    </Paper>
        <Typography variant="h6" gutterBottom className={classes.sidebarSection}>

    Recommended Code Viewers

</Typography>
    <Link display="block" variant="body1" href="https://js.do/" key = "">
        Javascript Code Viewer
    </Link>
    <Link display="block" variant="body1" href="https://repl.it/languages/java1" key = "">
        Java Code Viewer
    </Link>
    <Link display="block" variant="body1" href="https://dotnetfiddle.net/" key = "">
        C# and Visual Basic Viewer
    </Link>
    <Link display="block" variant="body1" href="https://repl.it/languages/python3" key = "">
        Python Viewer
    </Link>
    <Typography variant="h6" gutterBottom className={classes.sidebarSection}>
        Recommended IDEs that you should download for Readable code
    </Typography>
    <Link display="block" variant="body1" href="https://www.jetbrains.com/idea/download" key = "">
        Java - IntelliJ
    </Link>
    <Link display="block" variant="body1" href="#" key = "https://www.jetbrains.com/webstorm/download">
        JavaScript - Webstorm
    </Link>
    <Link display="block" variant="body1" href="https://www.jetbrains.com/pycharm/download" key = "">
        Python - PyCharm
    </Link>
    <Link display="block" variant="body1" href="https://www.jetbrains.com/rider/download" key = "">
        C# - Rider
    </Link>
</Grid>
    {/* End sidebar */}
</Grid>
    </main>
    </Container>
    {/* Footer */}
<footer className={classes.footer}>
        <Container maxWidth="lg">
        <Typography variant="h6" align="center" gutterBottom>
    Team members
    </Typography>
    <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
     Jonathan McCarte, Connor Kennedy, Eamon Fyfe, Ryan Barnes
    </Typography>
    <Copyright />
    </Container>
    </footer>
</React.Fragment>
);
}
