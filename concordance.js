// A2Z F17
// Daniel Shiffman
// http://shiffman.net/a2z
// https://github.com/shiffman/A2Z-F17

// An object to store all the info related to a concordance

class Concordance {
  constructor() {
    // this.dict = {};
    this.keys = [];
  }


  // Splitting up the text
  // getPositionInText(text, word) {
  //   return text.index(word);
  // }

  split(text) {
    // Split into array of tokens
    return text.split(/\W+/);
  }

  // A function to validate a toke
  validate(token) {
    return /\w{2,}/.test(token);
  }

  // Process new text
  process(data) {
    var tokens = this.split(data);
    // For every token
    for (var i = 0; i < tokens.length; i++) {
      // Lowercase everything to ignore case
      var token = tokens[i].toLowerCase();
      console.log(tokens[i+1]);
      if (this.validate(token)) {
        this.isThisVoid(token);
        this.isThisPrivate(token);
        this.isThisPublic(token);
        this.isVariableOkay(token, tokens[i+1]);
        this.isThisNull();
        //this.increment(token);
      }
    }
  }

  // An array of keys
  getKeys() {
    return this.keys;
  }

  // Get the count for a word
//   getCount(word) {
//     return this.dict[word];
// }

  isThisVoid(word) {
    if (word === "void") {
      console.log("WE FOUND A VOID!");
      this.keys.push("We found a void here, are you sure you dont need to return anything in this method?");
    }
  }

  // Increment the count for a word
  isThisPublic(word) {
    if (word === "public") {
      this.keys.push("We found you are using a public class, could this be private?");
    }
  }

  isThisPrivate(word) {
    if (word === "private") {
      this.keys.push("We found you are using a private class, are you sure you want to keep this private?");
    }
  }

  isThisNull(word) {
    if (word === "null") {
      this.keys.push("We found you have a null value in here, this could lead to errors, you sure this will be okay?");
    }
  }

  isVariableOkay(type, word) {
    if (type === "bool" || type === "var" || type === "int" || type === "string" || type === "let" || type === "double" || type === "float") {
      this.keys.push("We found that you have a " + type + " variable " + word +  " are you sure this is named correctly? Will everyone understand it?");
    }
  }

  // Sort array of keys by counts
  // sortByCount() {
  //   // For this function to work for sorting, I have
  //   // to store a reference to this so the context is not lost!
  //   var concordance = this;
  //
  //   // A fancy way to sort each element
  //   // Compare the counts
  //   function sorter(a, b) {
  //     var diff = concordance.getCount(b) - concordance.getCount(a);
  //     return diff;
  //   }
  //
  //   // Sort using the function above!
  //   this.keys.sort(sorter);
  // }



}
